import React, { Component } from 'react';
import { View, Text , Button} from 'react-native';
import { observer , inject} from 'mobx-react';

@inject('BaseStore')
@observer
class Page extends Component {
  constructor(props) {
    super(props);
    this.state = {
        name:'',
        username:'',
        password:'',
        fname:'',
        mname:'',
        gndr:'',
        cnumber:'',
        email:'',
        chosenDate:'',
        address:'',
        PickerValueHolder:'',
        pcode:'',
        iuser:'',
        fuser:'',
        coursename:'',
        qual:'',
        duration:'',
        hrname:'',
        collegeid:'',
        
    };
  }


  componentDidMount = () => {
        this.setState({name:this.props.BaseStore.infoStore.name});
        this.setState({username:this.props.BaseStore.infoStore.username});
        this.setState({password:this.props.BaseStore.infoStore.password});
        this.setState({fname:this.props.BaseStore.infoStore.fname});
        this.setState({mname:this.props.BaseStore.infoStore.mname});
        this.setState({gndr:this.props.BaseStore.infoStore.gndr});
        this.setState({cnumber:this.props.BaseStore.infoStore.cnumber});
        this.setState({email:this.props.BaseStore.infoStore.email});
        this.setState({chosenDate:this.props.BaseStore.infoStore.chosenDate});
        this.setState({address:this.props.BaseStore.infoStore.address});
        this.setState({PickerValueHolder:this.props.BaseStore.infoStore.PickerValueHolder});
        this.setState({pcode:this.props.BaseStore.infoStore.pcode});
        this.setState({iuser:this.props.BaseStore.infoStore.iuser});
        this.setState({fuser:this.props.BaseStore.infoStore.fuser});
        this.setState({coursename:this.props.BaseStore.infoStore.coursename});
        this.setState({collegeid:this.props.BaseStore.infoStore.collegeid});
        this.setState({qual:this.props.BaseStore.infoStore.qual});
        this.setState({duration:this.props.BaseStore.infoStore.duration});
        this.setState({hrname:this.props.BaseStore.infoStore.hrname});


       

        
  };
  render() {
    return (
      <View>
        <Text> Name : {this.state.name} </Text>
        <Text> Username : {this.state.username} </Text>
        <Text> Password : {this.state.password} </Text>
        <Text> Father Name : {this.state.fname} </Text>
        <Text> Mother Name : {this.state.mname} </Text>
        <Text> Gender : {this.state.gndr} </Text>
        <Text> Contact Number : {this.state.cnumber} </Text>
        <Text> Email : {this.state.email} </Text>
        <Text> Date : {this.state.chosenDate.toString().substr(4, 12)} </Text>
        <Text> Address : {this.state.address} </Text>
        <Text> State : {this.state.PickerValueHolder} </Text>
        <Text> Postal Code : {this.state.pcode} </Text>
        <Text> Instagram Username : {this.state.iuser} </Text>
        <Text> Facebook Username : {this.state.fuser} </Text>
        <Text> Technology : {this.state.coursename} </Text>
        <Text> College Name : {this.state.collegeid} </Text>        
        <Text> Qualification: {this.state.qual}</Text>
        <Text> Duration: {this.state.duration}</Text>
        <Text> HR Name: {this.state.hrname}</Text>


  
        <Button 
          title="Change Page"
          onPress={() => this.props.navigation.navigate('Page1')}
        />
      </View>
    );
  }
}

export default Page;
