import React, { Component } from 'react';
import { View, Text,ImageBackground,Image, KeyboardAvoidingView} from 'react-native';
import {Label,Item,Input,Button} from 'native-base';
import { observer, inject} from 'mobx-react';

@inject('BaseStore')
@observer
class Page19 extends Component {
  constructor(props) {
    super(props);
    this.rand = Math.floor((Math.random() * 7000) + 1000);
    this.state = {
        code:'',
        mobile:'',
        name:'',
        all:{},
        btn:false,
        isLoading:false,
    };
  }
  componentDidMount(){
    let a = this.props.BaseStore.infoStore;
    let mobile = this.props.BaseStore.infoStore.cnumber;
    let name = this.props.BaseStore.infoStore.name;
    this.setState({all:a, mobile:mobile, name:name},() => {
        this.sendmessage();
    });
  }
  sendmessage(){
    this.setState({isLoading:true});
    // let url = "http://login.yourbulksms.com/api/sendhttp.php?authkey=&mobiles="+this.state.mobile+"&message=Dear "+this.state.name+" Your verification code is "+this.rand+"&sender=SIMRAN&route=4&country=91";
    // fetch(url)
    // .then(() => {
    //       this.setState({isLoading:false});
    //     alert('Please Enter OTP send to your mobile.')
    // })
    // .catch((err) => {
    //       this.setState({isLoading:false});
    //     alert(err);
    // })
    alert('Yoyr OTP is' + this.rand)
}

  insertDataToDB(){
    let data = new FormData()
    for ( let key in this.state.all ) {
          data.append(key, this.state.all[key]);
      }
      //   data.append('hrid',this.state.all.hrid)
      //   data.append('contact',this.state.all.contact)
      fetch('http://sachtechsolution.pe.hu/api/insert_enquiry_data', {
          method: 'POST',
          body: data,
      })
      .then((dd) => dd.json())
      .then((data) => {
          if(data.status == 'success'){
              // alert('Data Inserted');
              this.setState({isLoading:false});
            //   this.props.navigation.navigate('Thanks');
            alert('Success');
          }else{
              alert('OOPS ERROR' + data.status);
              this.setState({isLoading:false});
          }
      })
      .catch((err) => {
          alert(err);
      })
}

  checkcode(code){
    this.setState({code:code}, () => {
        if(code.length == 4){
            if(code==this.rand){
                  this.setState({isLoading:true});
                  this.insertDataToDB();
            }else{
                alert('Sorry Wrong Input Please Try Again');
                this.setState({code:''});
            }
        }
    });
}
  render() {
      
    return (
        
      <ImageBackground  source={{ uri : 'http://i.imgur.com/zqocsVxg.jpg'}} style={{flex:1, padding:30}}
     
      >
          <Text>
              {this.rand}
          </Text>
          <Text>{JSON.stringify(this.state.all)}</Text>
          <View style={{backgroundColor:'white' , height:100 , width:100,borderRadius:50,alignSelf:'center',marginTop:90}}> 
   <Image source={{ uri : 'https://thumbs.dreamstime.com/b/sunset-logo-sun-over-water-minimalistic-design-79511068.jpg'}} style={{height:90 , width:90,borderRadius:50,alignSelf:'center',marginTop:5}}/>
</View>
<Text style={{color:'white',fontSize:15,alignSelf:'center',marginTop:10}}>
    {this.state.name}
</Text>

            <Item style={{alignSelf:'center', marginTop:25}} >

                  <Input placeholder='Enter Your OTP' placeholderTextColor="white" style={{alignSelf:'center',color:'#c1e7ea',textAlign:'center'}}
                onChangeText={(text) => this.checkcode(text)}
                value={this.state.code}
                  keyboardType='numeric'
                  maxLength={4}

                  />
              </Item>
              <View style={{alignSelf:'center',marginTop:30}}>
                  
              <Text style={{color:'#c1e7ea',fontSize:16,marginTop:10}}>
                  We've sent the OTP code to your mobile.
                         
              </Text>
              
              </View>

      </ImageBackground>
      
    );
  }
}

export default Page19;
