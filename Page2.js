import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { observer , inject, Provider} from 'mobx-react';
import App2 from './App';
import BaseStore from './Store'


class Page2 extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
    <Provider BaseStore={new BaseStore()}>
        <App2 />
    </Provider>
    );
  }
}

export default Page2;
