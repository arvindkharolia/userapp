import React, { Component } from 'react';
import { View, Text,ImageBackground,TouchableOpacity} from 'react-native';
import {Form,Item,Input, Container, Content,Button,Left,Right} from 'native-base';
import { observer , inject, Provider} from 'mobx-react';

@inject('BaseStore')
@observer

class Page1 extends Component {
  constructor(props) {
    super(props);
    this.state = {
        name:''
    };
  }
  async updatedata(){
    if (this.state.name == ''){
      alert('Name will be required')
    }else{
      await this.props.BaseStore.infoStore.setName(this.state.name);
    
      this.props.navigation.navigate('Page2')
    }
    
   
  }
  componentDidMount(){
   let a = this.props.BaseStore.infoStore.name
   this.setState({name:a});
  }


  render() {
    return (
    
        <ImageBackground source={{uri : 'https://png.pngtree.com/thumb_back/fw800/back_our/20190625/ourmid/pngtree-yellow-radial-gradient-background-image_258495.jpg'}} style={{height:"100%",justifyContent:'flex-end',}}>
          <View style={{padding:20}} >
           <View style={{}}>
           <Text style={{color:'white',fontSize:30,alignSelf:"center",marginBottom:10,fontWeight:'bold'}}>
            Tell Us Your Full Name
            </Text>
         
            
              <Item style={{backgroundColor:"white",borderRadius:10}}>
                <Input placeholder='Enter  Name'
                onChangeText={(text) => this.setState({name:text})}
                value={this.state.name}
                />
              </Item>
            
            </View>
            <View style={{flexDirection:'row', marginTop:30}}>
             
              
              <Right>
              <TouchableOpacity onPress={() => this.updatedata()} style={{backgroundColor:'green', paddingHorizontal:20, paddingVertical:10}}>
                <Text style={{color:"white"}}>
                   Next
                </Text>
              </TouchableOpacity>
              </Right>

            </View>
          </View>
        </ImageBackground>
              
            
     
    );
  }
}

export default Page1;

