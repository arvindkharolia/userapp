import React, { Component } from 'react';
import { View, Text,ImageBackground,TouchableOpacity} from 'react-native';
import {Form,Item,Input, Container, Content,Button,Left,Right,Textarea} from 'native-base';
import { observer, inject, Provider } from 'mobx-react';


@inject('BaseStore')
@observer

class Page10 extends Component {
  constructor(props) {
    super(props);
    this.state = {
        address:''
    };
  }
  async updataData(){
    if (this.state.address == ''){
      alert('Address will be Needed')
    }else{
      await this.props.BaseStore.infoStore.setAdres(this.state.address)
      this.props.navigation.navigate('Page11')
    }
   
  }
  componentDidMount(){
    let a = this.props.BaseStore.infoStore.address
    this.setState({address:a})
  }


  render() {
    return (
    
        <ImageBackground source={{uri : 'https://png.pngtree.com/thumb_back/fw800/background/20190221/ourmid/pngtree-abstract-rose-texture-3d-atmosphere-image_29266.jpg'}} style={{height:"100%",width:'100%',justifyContent:'flex-end'}}>
          <View style={{padding:20}}>
           <View >
           <Text style={{color:'white',fontSize:22,alignSelf:"center",marginBottom:10,fontWeight:'bold'}}>
           Tell Us Your Current Address
            </Text>
           <Textarea rowSpan={5} bordered placeholder="#845,XXX Nagar" 
           placeholderTextColor='grey'
           onChangeText={(text) => this.setState({address:text})}
           value={this.state.address}

           />
            
            </View>
            <View style={{flexDirection:'row',paddingTop:50}}>
              <Left>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('Page9')} style={{backgroundColor:'red', paddingHorizontal:20, paddingVertical:10}}>
                <Text style={{color:"white"}}>
                   Back
                </Text>
              </TouchableOpacity>
              </Left>
             
              <Right>
              <TouchableOpacity onPress={() => this.updataData()} style={{backgroundColor:'green', paddingHorizontal:20, paddingVertical:10}}>
                <Text style={{color:"white"}}>
                   Next
                </Text>
              </TouchableOpacity>
              </Right>

            </View>
          </View>
        </ImageBackground>
              
            
     
    );
  }
}

export default Page10;

