import React, { Component } from 'react';
import { View, Text,ImageBackground,TouchableOpacity,Picker,StyleSheet,ActivityIndicator, } from 'react-native';
import {Form,Item,Input, Container, Content,Button,Left,Right} from 'native-base';
import { observer, inject, Provider } from 'mobx-react';


@inject('BaseStore')
@observer

class Page11 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      

      PickerValueHolder: ''

    };
    
  }

  async updataData(){
    if (this.state.PickerValueHolder == ''){
      alert('States will be Needed')
    }else{
      await this.props.BaseStore.infoStore.setStates(this.state.PickerValueHolder)
      this.props.navigation.navigate('Page12')
    }
   
  }
  componentDidMount() {
   this.fetchdata();
}


componentDidMount(){
  let a = this.props.BaseStore.infoStore.PickerValueHolder
  this.setState({PickerValueHolder:a})
}
  render() {

    return (
    
        <ImageBackground source={{uri : 'https://png.pngtree.com/thumb_back/fw800/back_our/20190619/ourmid/pngtree-lantern-solid-color-simple-small-fresh-background-image_140927.jpg'}} style={{height:"100%",width:'100%',justifyContent:'flex-end'}}>
          <View style={{padding:20,marginBottom:10}}>
           <View >
           <Text style={{color:'white',fontSize:30,alignSelf:"center",marginBottom:10,fontWeight:'bold'}}>
           Tell Us Your State
            </Text>
           
           <View style={styles.MainContainer}>
              

                  
              <Picker
                  selectedValue={this.state.PickerValueHolder} style={{backgroundColor:'white'}}

                  onValueChange={(itemValue, itemIndex) => this.setState({ PickerValueHolder: itemValue })}
                value={this.state.PickerValueHolder}
                >
 <Picker.Item  label='Select' value='No Selected'/>
 <Picker.Item  label='Andhra Pradesh' value='Andhra Pradesh'/>
 <Picker.Item  label='Arunachal Pradesh' value='Arunachal Pradesh'/>
 <Picker.Item  label='Assam ' value='Assam '/>
 <Picker.Item  label='Bihar ' value='Bihar '/>
 <Picker.Item  label='Chhattisgarh ' value='Chhattisgarh '/>
 <Picker.Item  label='Chandigarh ' value='Chandigarh '/>
 <Picker.Item  label='Dadra and Nagar Haveli ' value='Dadra and Nagar Haveli '/>
 <Picker.Item  label="Daman and Diu" value="Daman and Diu"/>
 <Picker.Item  label='Delhi ' value='Delhi '/>
 <Picker.Item  label='Goa ' value='Goa '/>
 <Picker.Item  label='Gujarat ' value='Gujarat '/>
 <Picker.Item  label='Haryana ' value='Haryana '/>
 <Picker.Item  label='Himachal Pradesh' value='Himachal  Pradesh'/>
 <Picker.Item  label= 'Jammu and Kashmir' value='Jammu and Kashmir'/>
 <Picker.Item  label='Jharkhand ' value='Jharkhand '/>
 <Picker.Item  label='Karnataka ' value='Karnataka '/>
 <Picker.Item  label='Kerala ' value='Kerala '/>
 <Picker.Item  label='Madhya Pradesh' value='Madhya Pradesh'/>
 <Picker.Item  label='Maharashtra ' value='Maharashtra '/>
 <Picker.Item  label='Manipur ' value='Manipur '/>
 <Picker.Item  label='Meghalaya ' value='Meghalaya '/>
 <Picker.Item  label='Mizoram ' value='Mizoram '/>
 <Picker.Item  label='Nagaland ' value='Nagaland '/>
 <Picker.Item  label='Orissa ' value='Orissa '/>
 <Picker.Item  label='Punjab ' value='Punjab '/>
 <Picker.Item  label='Pondicherry ' value='Pondicherry '/>
 <Picker.Item  label='Rajasthan ' value='Rajasthan '/>
 <Picker.Item  label='Sikkim ' value='Sikkim '/>
 <Picker.Item  label='Tamil Nadu ' value='Tamil Nadu '/>
 <Picker.Item  label='Tripura ' value='Tripura '/>
 <Picker.Item  label='Uttar Pradesh ' value='Uttar Pradesh '/>
 <Picker.Item  label='Uttarakhand ' value='Uttarakhand '/>
 <Picker.Item  label='West Bengal' value='West Bengal'/>

 









              </Picker>
      

          </View>
            </View>
            <View style={{flexDirection:'row',paddingTop:50}}>
              <Left>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('Page10')} style={{backgroundColor:'red', paddingHorizontal:20, paddingVertical:10}}>
                <Text style={{color:"white"}}>
                   Back
                </Text>
              </TouchableOpacity>
              </Left>
            
              <Right>
              <TouchableOpacity onPress={() => this.updataData()} style={{backgroundColor:'green', paddingHorizontal:20, paddingVertical:10}}>
                <Text style={{color:"white"}}>
                   Next
                </Text>
              </TouchableOpacity>
              </Right>

            </View>
          </View>
        </ImageBackground>
              
            
     
    );
  }
}

const styles = StyleSheet.create({

  MainContainer: {

      justifyContent: 'center',
      flex: 1,
      margin: 10
  }

});

export default Page11;

