import React, { Component } from 'react';
import { View, Text,ImageBackground,TouchableOpacity} from 'react-native';
import {Form,Item,Input, Container, Content,Button,Left,Right,Textarea} from 'native-base';
import { observer, inject, Provider } from 'mobx-react';


@inject('BaseStore')
@observer

class Page12 extends Component {
  constructor(props) {
    super(props);
    this.state = {
        pcode:''
    };
  }
  async updataData(){
    if (this.state.pcode == ''){
      alert('Pin code will be Needed')
    }else{
      await this.props.BaseStore.infoStore.setPcode(this.state.pcode)
      this.props.navigation.navigate('Page13')
    }
    
  }
  componentDidMount(){
    let a = this.props.BaseStore.infoStore.pcode
    this.setState({pcode:a})
  }

  render() {
    return (
    
        <ImageBackground source={{uri : 'https://png.pngtree.com/thumb_back/fw800/background/20190223/ourmid/pngtree-texture-hand-drawn-gradient-poster-background-backgroundgradienthand-paintedwatercolorbackgroundh5-background-image_67235.jpg'}} style={{height:"100%",width:'100%',justifyContent:'flex-end'}}>
          <View style={{padding:20}}>
           <View >
           <Text style={{color:'white',fontSize:25,alignSelf:"center",marginBottom:10,fontWeight:'bold'}}>
           Tell Us Your Postal Code
            </Text>
           
                <Item style={{backgroundColor:'white',borderRadius:10}}>
                    <Input  placeholder="XXXXXX"  
                    maxLength={6}
                    keyboardType='numeric'
                    onChangeText={(text) => this.setState({pcode:text})}
                value={this.state.pcode}
                />
                </Item>
        
           
            
            </View>
            <View style={{flexDirection:'row',paddingTop:50}}>
              <Left>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('Page11')} style={{backgroundColor:'red', paddingHorizontal:20, paddingVertical:10}}>
                <Text style={{color:"white"}}>
                   Back
                </Text>
              </TouchableOpacity>
              </Left>
             
              <Right>
              <TouchableOpacity onPress={() => this.updataData()} style={{backgroundColor:'green', paddingHorizontal:20, paddingVertical:10}}>
                <Text style={{color:"white"}}>
                   Next
                </Text>
              </TouchableOpacity>
              </Right>

            </View>
          </View>
        </ImageBackground>
              
            
     
    );
  }
}

export default Page12;

