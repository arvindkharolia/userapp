import React, { Component } from 'react';
import { View, Text,ImageBackground,TouchableOpacity} from 'react-native';
import {Form,Item,Input, Container, Content,Button,Left,Right,Label} from 'native-base';
import { observer, inject, Provider } from 'mobx-react';


@inject('BaseStore')
@observer

class Page13 extends Component {
  constructor(props) {
    super(props);
    this.state = {
        iuser:'',
        fuser:''
    };
  }
  async updateData(){
    if (this.state.address == ''){
      alert('Address will be Needed')
    }else{
      await this.props.BaseStore.infoStore.setIuser(this.state.iuser);
      await this.props.BaseStore.infoStore.setFuser(this.state.fuser);
      this.props.navigation.navigate('Page14')
    }
  

  }
  componentDidMount(){
    let a = this.props.BaseStore.infoStore.iuser
    let b = this.props.BaseStore.infoStore.fuser
    this.setState({iuser:a,fuser:b})
  }

  render() {
    return (
    
        <ImageBackground source={{uri : 'https://png.pngtree.com/thumb_back/fw800/background/20190223/ourmid/pngtree-texture-hand-drawn-gradient-poster-background-backgroundgradienthand-paintedwatercolorbackgroundh5-background-image_67235.jpg'}} style={{justifyContent:'flex-end',height:"100%",width:'100%'}}>
          <View style={{padding:20}}>
           <View >
           <Label style={{color:'white',fontSize:30,alignSelf:"center",marginBottom:10,fontWeight:'bold'}}>
           Instagram Username
            </Label>
           
                <Item style={{backgroundColor:'white',borderRadius:10}}>
                    <Input  placeholder=" "  onChangeText={(text) => this.setState({iuser:text})}
                value={this.state.iuser}                    
                    />
                </Item>
           
            <Label style={{color:'white',fontSize:30,alignSelf:"center",marginBottom:10,fontWeight:'bold'}}>
           Facebook Username
            </Label>
            
                <Item style={{backgroundColor:'white',borderRadius:10}}>
                    <Input  placeholder=" "  onChangeText={(text) => this.setState({fuser:text})}
                value={this.state.fuser}
                />
                </Item>
            
           
            
            </View>
            <View style={{flexDirection:'row',paddingTop:50}}>
              <Left>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('Page12')} style={{backgroundColor:'red', paddingHorizontal:20, paddingVertical:10}}>
                <Text style={{color:"white"}}>
                   Back
                </Text>
              </TouchableOpacity>
              </Left>
            
              <Right>
              <TouchableOpacity onPress={() =>  this.updateData()} style={{backgroundColor:'green', paddingHorizontal:20, paddingVertical:10}}>
                <Text style={{color:"white"}}>
                   Next
                </Text>
              </TouchableOpacity>
              </Right>

            </View>
          </View>
        </ImageBackground>
              
            
     
    );
  }
}

export default Page13;

