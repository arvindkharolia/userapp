import React, { Component } from 'react';
import { View, Text, ImageBackground, TouchableOpacity, Picker, PickerItem } from 'react-native';
import { Form, Item, Input, Container, Content, Button, Left, Right ,Spinner} from 'native-base';
import { observer, inject, Provider } from 'mobx-react';


@inject('BaseStore')
@observer

class Page14 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      coursename: '1',
      isLoading:false,
      data: [],

    };
    this.arr = [];
  }
  fetchApi() {
    this.setState({isLoading:true});

    fetch('http://sachtechsolution.pe.hu/api/getcourses')
      .then((data) => data.json())
      .then((res) => {
        let a = res.data
        this.arr = a;
        this.setState({ abcd: '' })
    this.setState({isLoading:false})

      })
  }
  async updateData() {
    await this.props.BaseStore.infoStore.setTech(this.state.coursename);
    this.props.navigation.navigate('Page15')
  }

  componentDidMount() {
   
   
   
  
    
    this.fetchApi();
  }

  render() {
    
    if(this.state.isLoading){
      return(
      <View style={{marginTop:250}}>
        <Spinner/>

        <Text style={{alignSelf:'center',fontSize:20}}>Loading...</Text>
          
       
      </View>
      )
    }

    return (

      <ImageBackground source={{ uri: 'https://png.pngtree.com/thumb_back/fw800/background/20190223/ourmid/pngtree-simple-green-solid-background-colorsimple-image_87274.jpg' }} style={{ height: "100%", width: '100%',justifyContent:'flex-end' }}>
        <View style={{padding:20}}>
          <View >
            <Text style={{ color: 'white', fontSize: 30, alignSelf: "center", marginBottom: 10, fontWeight: 'bold' }}>
              Technology
            </Text>
            <Picker 
              selectedValue={this.state.coursename}
              style={{backgroundColor:'white'}}
              onValueChange={(itemValue, itemIndex) => this.setState({ coursename: itemValue })} 
              value={this.state.coursename}
              
              >

              {this.arr.map((item, key) => {
                return (
                  <Picker.Item label={item.course_name} value={item.course_name} key={key} />)
              }
              )}

            </Picker>
          </View>
          <View style={{ flexDirection: 'row', paddingTop: 50 }}>
            <Left>
              <TouchableOpacity onPress={() => this.props.navigation.navigate('Page13')} style={{backgroundColor:'red', paddingHorizontal:20, paddingVertical:10}}>
                <Text style={{ color: "white" }}>
                  Back
                </Text>
              </TouchableOpacity>
            </Left>

            <Right>
              <TouchableOpacity onPress={() => this.updateData()} style={{backgroundColor:'green', paddingHorizontal:20, paddingVertical:10}}>
                <Text style={{ color: "white" }}>
                  Next
                </Text>
              </TouchableOpacity>
            </Right>

          </View>
        </View>
      </ImageBackground>



    );
  }
}

export default Page14;

