import React, { Component } from 'react';
import { View, Text, ImageBackground, TouchableOpacity, Picker, PickerItem } from 'react-native';
import { Left, Right,Spinner } from 'native-base';
import { observer, inject, Provider } from 'mobx-react';


@inject('BaseStore')
@observer


class Page15 extends Component {
  constructor(props) {
    super(props);
    this.state = {
        data:[],
        isLoading:false,
        collegeid:''
    };  
    this.arr = [];
  }

  getchData(){
    this.setState({isLoading:true});
    fetch('http://sachtechsolution.pe.hu/api/getcolleges')
    .then((data) => data.json())
    .then((res) => {
      let a = res.data
      this.arr = a;
      this.setState({abcd:''});
      this.setState({isLoading:false});
    })
  }
  async updateData(){
    await this.props.BaseStore.infoStore.setCol(this.state.collegeid)
    this.props.navigation.navigate('Page16')
  }
  makepickers(){ 
      return this.arr.map((item) => {
        return(
          <Picker.Item  label={item.college_name} value={item.id}/>
        )
      })
    
  }
  
  componentDidMount(){
    let a = this.props.BaseStore.infoStore.collegeid
    this.setState({collegeid:a})
 
  
      this.getchData()
  }
  render() {
    if(this.state.isLoading){
      return(
      <View style={{marginTop:250}}>
        <Spinner/>

        <Text style={{alignSelf:'center',fontSize:20}}>Loading...</Text>
          
       
      </View>
      )
    }
      return (
        <ImageBackground source={{ uri :'https://png.pngtree.com/thumb_back/fw800/background/20190223/ourmid/pngtree-solid-color-matte-background-blue-gradient-wind-background-color-mattesolid-backgroundblue-image_84871.jpg'}} style={{height:
        "100%",width:"100%",justifyContent:'flex-end'}}>  
       <View style={{ padding: 20, }}>
             <View >
               <Text style={{ color: 'white', fontSize: 25, alignSelf: "center", marginBottom: 10, fontWeight: 'bold' }}>
                 Select Your College Name
               </Text>
            
               <Picker
                     selectedValue={this.state.collegeid}
        
                     onValueChange={(itemValue, itemIndex) => this.setState({ collegeid: itemValue })} 
                   value={this.state.collegeid}
                     
                     >
                       {this.makepickers()}
   
                 </Picker>
                 
             </View>
             <View style={{ flexDirection: 'row', paddingTop: 50 }}>
               <Left>
                 <TouchableOpacity onPress={() => this.props.navigation.navigate('Page14')} style={{backgroundColor:'red', paddingHorizontal:20, paddingVertical:10}}>
                   <Text style={{ color: "white" }}>
                     Back
                   </Text>
                 </TouchableOpacity>
               </Left>
               
               <Right>
                 <TouchableOpacity onPress={() => this.updateData()} style={{backgroundColor:'green', paddingHorizontal:20, paddingVertical:10}}>
                   <Text style={{ color: "white" }}>
                     Next
                   </Text>
                 </TouchableOpacity>
               </Right>
   
             </View>
           </View>
          
        </ImageBackground>
        );
    
      

      
    
    
    
  }
}

export default Page15;
