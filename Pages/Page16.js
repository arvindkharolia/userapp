import React, { Component } from 'react';
import { View, Text, ImageBackground, TouchableOpacity, Picker } from 'react-native';
import { Left, Right } from 'native-base';
import { observer, inject, Provider } from 'mobx-react';


@inject('BaseStore')
@observer
class Page16 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      qual:'',

    };

  }
  async updateData(){
    await this.props.BaseStore.infoStore.setQual(this.state.qual)
   this.props.navigation.navigate('Page17')
   
  }
  componentDidMount(){
    let a = this.props.BaseStore.infoStore.qual
    this.setState({qual:a})
  }




  render() {

    return (

      <ImageBackground source={{ uri: 'https://png.pngtree.com/thumb_back/fw800/background/20190222/ourmid/pngtree-atmospheric-orange-gradient-cartoon-abstract-background-pageposter-background-image_50186.jpg' }} style={{ flex:1 ,height: "100%", width: '100%',justifyContent:'flex-end' }}>
        <View style={{padding:20}}>
          <View >
            <Text style={{ color: 'white', fontSize: 30, alignSelf: "center", marginBottom: 10, fontWeight: 'bold' }}>
              Qualification
            </Text>
            <Picker selectedValue={this.state.qual}
style={{backgroundColor:'white'}}
                  onValueChange={(itemValue, itemIndex) =>
                    this.setState({ qual: itemValue })
                  }
                  value={this.state.qual}
                

                >
                  <Picker.Item label='Select' value='No Selected' />                  
                  <Picker.Item label="B.Tech(CSE)" value="B.Tech(CSE)" />
                <Picker.Item label="B.Tech(ECE)" value="B.Tech(ECE)" />
                <Picker.Item label="B.Tech(IT)" value="B.Tech(IT)" />
                <Picker.Item label="BE(CSE)" value="BE(CSE)" />
                <Picker.Item label="BE(ECE)" value="BE(ECE)" />
                <Picker.Item label="BE(IT)" value="BE(IT)" />
                <Picker.Item label="B.Sc(IT)" value="B.Sc(IT)" />
                <Picker.Item label="B.Sc(CS)" value="B.Sc(CS)" />
                <Picker.Item label="BCA" value="BCA" />
                <Picker.Item label="MCA" value="MCA" />
                <Picker.Item label="M.Sc(IT)" value="M.Sc(IT)" />
                <Picker.Item label="M.Sc(CS)" value="M.Sc(CS)" />
                <Picker.Item label="PGDCA" value="PGDCA" />
                <Picker.Item label="B.Com" value="B.Com" />
                <Picker.Item label="BBA" value="BBA" />
                <Picker.Item label="MBA" value="MBA" />
                <Picker.Item label="Diploma(CSE)" value="Diploma(CSE)" />
                <Picker.Item label="Diploma(IT)" value="Diploma(IT)" />
                <Picker.Item label="Diploma(ECE)" value="Diploma(ECE)" />
                </Picker>

          </View>
          <View style={{ flexDirection: 'row', paddingTop: 50 }}>
            <Left>
              <TouchableOpacity onPress={() => this.props.navigation.navigate('Page15')} style={{backgroundColor:'red', paddingHorizontal:20, paddingVertical:10}}>
                <Text style={{ color: "white" }}>
                  Back
                </Text>
              </TouchableOpacity>
            </Left>
            
            <Right>
              <TouchableOpacity onPress={() => this.updateData()} style={{backgroundColor:'green', paddingHorizontal:20, paddingVertical:10}}>
                <Text style={{ color: "white" }}>
                  Next
                </Text>
              </TouchableOpacity>
            </Right>

          </View>
        </View>
      </ImageBackground>



    );
  }
}

export default Page16;

