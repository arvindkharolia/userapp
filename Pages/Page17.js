import React, { Component } from 'react';
import { View, Text, ImageBackground, TouchableOpacity, Picker, PickerItem } from 'react-native';
import { Left, Right } from 'native-base';
import { observer, inject, Provider } from 'mobx-react';


@inject('BaseStore')
@observer

class Page17 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      duration: '',

    };

  }
 async updateData(){
   await this.props.BaseStore.infoStore.setDur(this.state.duration)
   this.props.navigation.navigate('Page18')
 }
 componentDidMount(){
  let a = this.props.BaseStore.infoStore.duration
  this.setState({duration:a})


    
}

  render() {

    return (

      <ImageBackground source={{ uri: 'https://png.pngtree.com/thumb_back/fw800/background/20190221/ourmid/pngtree-yellow-gradient-pseudo-3d-stereoscopic-rose-texture-image_29276.jpg' }} style={{ flex:1 ,height: "100%", width: '100%',justifyContent:'flex-end' }}>
        <View style={{padding: 20, }}>
          <View >
            <Text style={{ color: 'white', fontSize: 30, alignSelf: "center", marginBottom: 10, fontWeight: 'bold' }}>
              Duration
            </Text>
            <Picker selectedValue={this.state.duration}
                  style={{backgroundColor:'white'}}
                  onValueChange={(itemValue, itemIndex) =>
                    this.setState({ duration: itemValue })
                  }
                value={this.state.duration}

                >
                  <PickerItem label='Select' value='No Selected' />                  
                  <Picker.Item label="6 Weeks" value="6 Weeks" />
                <Picker.Item label="3 Months" value="3 Months" />
                <Picker.Item label="6 Months" value="6 Months" />
                </Picker>

          </View>
          <View style={{ flexDirection: 'row', paddingTop: 50 }}>
            <Left>
              <TouchableOpacity onPress={() => this.props.navigation.navigate('Page16')} style={{backgroundColor:'red', paddingHorizontal:20, paddingVertical:10}}>
                <Text style={{ color: "white" }}>
                  Back
                </Text>
              </TouchableOpacity>
            </Left>
            
            <Right>
              <TouchableOpacity onPress={() => this.updateData()} style={{backgroundColor:'green', paddingHorizontal:20, paddingVertical:10}}>
                <Text style={{ color: "white" }}>
                  Next
                </Text>
              </TouchableOpacity>
            </Right>

          </View>
        </View>
      </ImageBackground>



    );
  }
}

export default Page17;

