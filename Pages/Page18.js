import React, { Component } from 'react';
import { View, Text, ImageBackground, TouchableOpacity, Picker, PickerItem } from 'react-native';
import { Form, Item, Input, Container, Content, Button, Left, Right,Spinner } from 'native-base';
import { observer, inject, Provider } from 'mobx-react';


@inject('BaseStore')
@observer

class Page18 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      hrname: '',
      isLoading:false,
      data: [],

    };
    this.arr = [];
  }
  fetchApi() {
    this.setState({isLoading:true});
    fetch('http://sachtechsolution.pe.hu/api/getmarketing')
      .then((data) => data.json())
      .then((res) => {
        let a = res.data
        this.arr = a;
        this.setState({ abcd: '' })
    this.setState({isLoading:false});
      })
  }
  async updateData() {
    await this.props.BaseStore.infoStore.setHr(this.state.hrname);
    this.props.navigation.navigate('Page19')
  }

  componentDidMount() {
    
     
    this.fetchApi();
  }

  render() {
    if(this.state.isLoading){
      return(
      <View style={{marginTop:250}}>
        <Spinner/>

        <Text style={{alignSelf:'center',fontSize:20}}>Loading...</Text>
          
       
      </View>
      )
    }

    return (

      <ImageBackground source={{ uri: 'https://png.pngtree.com/thumb_back/fw800/back_our/20190621/ourmid/pngtree-cute-children-s-solid-color-animal-background-border-image_183900.jpg' }} style={{ height: "100%", width: '100%',justifyContent:'flex-end'}}>
        <View style={{padding:20}}>
          <View >
            <Text style={{ color: 'white', fontSize: 30, alignSelf: "center", marginBottom: 10, fontWeight: 'bold' }}>
             Enter HR Name
            </Text>
            <Picker
              selectedValue={this.state.hrname} style={{backgroundColor:'white'}}

              onValueChange={(itemValue, itemIndex) => this.setState({ hrname: itemValue })} 
              value={this.state.hrname}
              >

              {this.arr.map((item, key) => {
                return (
                    
                  <Picker.Item label={item.pre_title +'. ' + item.first_name + ' '+ item.last_name} value={item.id} key={key} />)
              }
              )}

            </Picker>
          </View>
          <View style={{ flexDirection: 'row', paddingTop: 50 }}>
            <Left>
              <TouchableOpacity onPress={() => this.props.navigation.navigate('Page17')} style={{backgroundColor:'red', paddingHorizontal:20, paddingVertical:10}}>
                <Text style={{ color: "white" }}>
                  Back
                </Text>
              </TouchableOpacity>
            </Left>

            <Right>
              <TouchableOpacity onPress={() => this.updateData()} style={{backgroundColor:'green', paddingHorizontal:20, paddingVertical:10}}>
                <Text style={{ color: "white" }}>
                  Next
                </Text>
              </TouchableOpacity>
            </Right>

          </View>
        </View>
      </ImageBackground>



    );
  }
}

export default Page18;

