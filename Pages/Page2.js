import React, { Component } from 'react';
import { View, Text,ImageBackground,TouchableOpacity} from 'react-native';
import {Form,Item,Input, Container,Button,Left,Right} from 'native-base';
import { observer , inject, Provider} from 'mobx-react';

@inject('BaseStore')
@observer
class Page2 extends Component {
  constructor(props) {
    super(props);
    this.state = {
        username:''
    };
  }
  async updatedata(){
    if (this.state.username == ''){
      alert('Username will be required')
    }else{
      await this.props.BaseStore.infoStore.setUsername(this.state.username)  
      this.props.navigation.navigate('Page3')
    }
    
    
  }
  componentDidMount(){
    let a = this.props.BaseStore.infoStore.username;
    this.setState({username:a});
  }
  render() {
    return (
    
        <ImageBackground source={{uri : 'https://png.pngtree.com/thumb_back/fw800/background/20190223/ourmid/pngtree-solid-color-space-3d-blue-cool-background-colorspacecoolblue-image_83754.jpg'}} style={{height:"100%",width:'100%',justifyContent:'flex-end'}}>
          <View style={{padding:20}}>
           <View >
           <Text style={{color:'white',fontSize:30,alignSelf:"center",marginBottom:10,fontWeight:'bold'}}>
           Enter Your Username
            </Text>
         
           
              <Item style={{backgroundColor:"white",borderRadius:10}}>
                <Input placeholder='Enter  Username'
                onChangeText={(text) => this.setState({username:text})}
                value={this.state.username}
                />
              </Item>
           
            </View>
            <View style={{flexDirection:'row',paddingTop:30}}>
              <Left>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('Page1')} style={{backgroundColor:'red', paddingHorizontal:20, paddingVertical:10}}>
                <Text style={{color:"white"}}>
                   Back
                </Text>
              </TouchableOpacity>
              </Left>
              
              <Right>
              <TouchableOpacity onPress={() => this.updatedata()} style={{backgroundColor:'green', paddingHorizontal:20, paddingVertical:10}}>
                <Text style={{color:"white"}}>
                   Next
                </Text>
              </TouchableOpacity>
              </Right>

            </View>
          </View>
        </ImageBackground>
              
            
     
    );
  }
}

export default Page2;

