import React, { Component } from 'react';
import { View, Text,ImageBackground,TouchableOpacity} from 'react-native';
import {Form,Item,Input, Container, Content,Button,Left,Right} from 'native-base';
import { observer, inject, Provider } from 'mobx-react';


@inject('BaseStore')
@observer
class Page3 extends Component {
  constructor(props) {
    super(props);
    this.state = {
        password:''
    };
  }
  validateEmail(password) {
    var re = /^[A-Za-z]\w{7,15}$/;
    return re.test(String(password).toLowerCase());
  }
  async updateData(){
    if(this.validateEmail(this.state.password)){
    if (this.state.password == ''){
      alert('Please Fill Password');
    }else{
      await this.props.BaseStore.infoStore.setPassword(this.state.password)
    
      this.props.navigation.navigate('Page4')
    }
  }else{
    alert('Password Not Valid');
  }
   
    
  }
  componentDidMount(){
    let a = this.props.BaseStore.infoStore.password
    this.setState({password:a})
  }

  render() {
    return (
    
        <ImageBackground source={{uri : 'https://png.pngtree.com/thumb_back/fw800/background/20190223/ourmid/pngtree-texture-hand-drawn-gradient-poster-background-backgroundgradienthand-paintedwatercolorbackgroundh5-background-image_67235.jpg'}} style={{height:"100%",width:'100%',justifyContent:'flex-end'}}>
          <View style={{padding:20}}>
           <View >
           <Text style={{color:'white',fontSize:31,alignSelf:"center",marginBottom:10,fontWeight:'bold'}}>
           Enter Your Password
            </Text>
         
         
              <Item style={{backgroundColor:"white",borderRadius:10}}>
                <Input placeholder='Enter  Password' secureTextEntry
                minLength='8'
                
                onChangeText={(text) => this.setState({password:text})} 
                value={this.state.password}

                />
              </Item>
         
            </View>
            <View style={{flexDirection:'row',paddingTop:30}} > 
              <Left>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('Page2')} style={{backgroundColor:'red', paddingHorizontal:20, paddingVertical:10}}>
                <Text style={{color:"white"}}>
                   Back
                </Text>
              </TouchableOpacity>
              </Left>
              
              <Right>
              <TouchableOpacity onPress={() => this.updateData()} style={{backgroundColor:'green', paddingHorizontal:20, paddingVertical:10}}>
                <Text style={{color:"white"}}>
                   Next
                </Text>
              </TouchableOpacity>
              </Right>

            </View>
          </View>
        </ImageBackground>
              
            
     
    );
  }
}

export default Page3;

