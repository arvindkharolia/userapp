import React, { Component } from 'react';
import { View, Text,ImageBackground,TouchableOpacity} from 'react-native';
import {Form,Item,Input, Container, Content,Button,Left,Right} from 'native-base';
import { observer, inject, Provider } from 'mobx-react';


@inject('BaseStore')
@observer

class Page3 extends Component {
  constructor(props) {
    super(props);
    this.state = {
        fname:''
    };
  }
  async updateData(){
    if (this.state.fname == ''){
      alert('Father Name will be Needed')
    }else{
      await this.props.BaseStore.infoStore.setFname(this.state.fname)
    this.props.navigation.navigate('Page5')
    } 
   
  }
  componentDidMount(){
    let a = this.props.BaseStore.infoStore.fname
    this.setState({fname:a})
  }

  render() {
    return (
    
        <ImageBackground source={{uri : 'https://png.pngtree.com/thumb_back/fw800/background/20190223/ourmid/pngtree-full-warm-color-gradient-fashion-texture-color-brush-watercolor-background-material-image_69880.jpg'}} style={{justifyContent:'flex-end',height:"100%",width:'100%'}}>
          <View style={{padding:20}}>
           <View >
           <Text style={{color:'white',fontSize:22,alignSelf:"center",marginBottom:10,fontWeight:'bold'}}>
           Tell Us Enter Your Father Name
            </Text>
         
         
              <Item style={{backgroundColor:"white",borderRadius:10}}>
                <Input placeholder='Enter Father Name' 
                onChangeText={(text) => this.setState({fname:text})} 
                value={this.state.fname}

                />
              </Item>
           
            </View>
            <View style={{flexDirection:'row',paddingTop:50}}>
              <Left>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('Page3')} style={{backgroundColor:'red', paddingHorizontal:20, paddingVertical:10}}>
                <Text style={{color:"white"}}>
                   Back
                </Text>
              </TouchableOpacity>
              </Left>
              
              <Right>
              <TouchableOpacity onPress={() => this.updateData()} style={{backgroundColor:'green', paddingHorizontal:20, paddingVertical:10}}>
                <Text style={{color:"white"}}>
                   Next
                </Text>
              </TouchableOpacity>
              </Right>

            </View>
          </View>
        </ImageBackground>
              
            
     
    );
  }
}

export default Page3;

