import React, { Component } from 'react';
import { View, Text,ImageBackground,TouchableOpacity} from 'react-native';
import {Form,Item,Input, Container, Content,Button,Left,Right} from 'native-base';
import { observer, inject, Provider } from 'mobx-react';


@inject('BaseStore')
@observer

class Page5 extends Component {
  constructor(props) {
    super(props);
    this.state = {
        mname:''
    };
  }
  async updataData(){
    if (this.state.mname == ''){
      alert('Mother Name will be Needed')
    }else{
      await this.props.BaseStore.infoStore.setMname(this.state.mname)
    this.props.navigation.navigate('Page6')
    }
    
  }
  componentDidMount(){
    let a = this.props.BaseStore.infoStore.mname
    this.setState({mname:a})
  }

  render() {
    return (
        <ImageBackground source={{uri : 'https://png.pngtree.com/thumb_back/fw800/back_pic/04/16/66/89582584af17bc8.jpg'}} style={{height:"100%",width:'100%',justifyContent:'flex-end'}}>
          <View style={{padding:20}}>
           <View >
           <Text style={{color:'white',fontSize:22,alignSelf:"center",marginBottom:10,fontWeight:'bold'}}>
           Tell Us Your Mother Name
            </Text>
         
              <Item style={{backgroundColor:"white",borderRadius:10}}>
                <Input placeholder='Enter Mother Name' 
                onChangeText={(text) => this.setState({mname:text})} 
                value={this.state.mname}
                />
              </Item>
           
            </View>
            <View style={{flexDirection:'row',paddingTop:50}}>
              <Left>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('Page4')} style={{backgroundColor:'red', paddingHorizontal:20, paddingVertical:10}}>
                <Text style={{color:"white"}}>
                   Back
                </Text>
              </TouchableOpacity>
              </Left>
             
              <Right>
              <TouchableOpacity onPress={() => this.updataData()} style={{backgroundColor:'green', paddingHorizontal:20, paddingVertical:10}}>
                <Text style={{color:"white"}}>
                   Next
                </Text>
              </TouchableOpacity>
              </Right>

            </View>
          </View>
        </ImageBackground>
              
            
     
    );
  }
}

export default Page5;

