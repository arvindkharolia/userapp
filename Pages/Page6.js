import React, { Component } from 'react';
import { View, Text,ImageBackground,TouchableOpacity,Picker,PickerItem} from 'react-native';
import {Form,Item,Input, Container, Content,Button,Left,Right} from 'native-base';
import { observer, inject, Provider } from 'mobx-react';


@inject('BaseStore')
@observer

class Page6 extends Component {
  constructor(props) {
    super(props);
    this.state = {
        gndr:'No Selected'
    };
  }
  async updataData(){
    if (this.state.gndr == 'No Selected'){
      alert('Gender will be Needed')
    }else{
      await this.props.BaseStore.infoStore.setGndr(this.state.gndr)
      this.props.navigation.navigate('Page7')
    }
   
  }
  componentDidMount(){
    let a = this.props.BaseStore.infoStore.gndr
    this.setState({gndr:a})
  }


  render() {
    return (
    
        <ImageBackground source={{uri : 'https://png.pngtree.com/thumb_back/fw800/back_pic/03/94/51/0657e9de3dcb6a8.jpg'}} style={{height:"100%",width:'100%',justifyContent:'flex-end'}}>
          <View style={{padding:20}}>
           <View>
           <Text style={{color:'white',fontSize:35,alignSelf:"center",marginBottom:10,fontWeight:'bold'}}>
           Enter Your Gender
            </Text>
         
             <Item style={{backgroundColor:"white",borderRadius:10}}>
             <Picker
  selectedValue={this.state.gndr}
  style={{height: 50, width: 200,backgroundColor:'white'}}
  onValueChange={(itemValue, itemIndex) =>
    this.setState({gndr: itemValue})
    
  }
  value={this.state.gndr}>
  <Picker.Item label="Select" value="No Selected" />
      
  <Picker.Item label="Male" value="Male" />
  <Picker.Item label="Female" value="Female" />
</Picker>
             </Item>
         
         
            </View>
            <View style={{flexDirection:'row',paddingTop:50}}>
              <Left>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('Page5')} style={{backgroundColor:'red', paddingHorizontal:20, paddingVertical:10}}>
                <Text style={{color:"white"}}>
                   Back
                </Text>
              </TouchableOpacity>
              </Left>
              
              <Right>
              <TouchableOpacity onPress={() => this.updataData()} style={{backgroundColor:'green', paddingHorizontal:20, paddingVertical:10}}>
                <Text style={{color:"white"}}>
                   Next
                </Text>
              </TouchableOpacity>
              </Right>

            </View>
          </View>
        </ImageBackground>
              
            
     
    );
  }
}

export default Page6;

