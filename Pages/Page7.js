import React, { Component } from 'react';
import { View, Text,ImageBackground,TouchableOpacity} from 'react-native';
import {Form,Item,Input, Container, Content,Button,Left,Right} from 'native-base';
import { observer, inject, Provider } from 'mobx-react';


@inject('BaseStore')
@observer

class Page7 extends Component {
  constructor(props) {
    super(props);
    this.state = {
        cnumber:''
    };
  }
  
  async updataData(){
    if(this.state.cnumber.length <= 9){
      alert('Contact Number Not Valid')
    } else{
        await this.props.BaseStore.infoStore.setCno(this.state.cnumber)
        this.props.navigation.navigate('Page8')
      
      
      
      
    }
 
   
  
  }
  componentDidMount(){
    let a = this.props.BaseStore.infoStore.cnumber
    this.setState({cnumber:a})
  }


  render() {
    return (
    
        <ImageBackground source={{uri : 'https://png.pngtree.com/thumb_back/fw800/background/20190223/ourmid/pngtree-geometric-low-polygon-yellow-orange-minimalistic-romantic-gradient-background-material-polygonyellow-image_66646.jpg'}} style={{justifyContent:'flex-end',height:"100%",width:'100%'}}>
          <View style={{padding:20}}>
           <View>
           <Text style={{color:'white',fontSize:30,alignSelf:"center",marginBottom:10,fontWeight:'bold'}}>
           Tell Us Your Contact Number
            </Text>
         
            
              <Item style={{backgroundColor:"white",borderRadius:10}}>
                <Input placeholder='+9197585XXXXX' 
                keyboardType='numeric'
                maxLength={10}
              
                onChangeText={(text) => this.setState({cnumber:text})} 
                value={this.state.cnumber}

                />
              </Item>
           
            </View>
            <View style={{flexDirection:'row',paddingTop:50}}>
              <Left>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('Page6')} style={{backgroundColor:'red', paddingHorizontal:20, paddingVertical:10}}>
                <Text style={{color:"white"}}>
                   Back
                </Text>
              </TouchableOpacity>
              </Left>
              
              <Right>
              <TouchableOpacity onPress={() => this.updataData()} style={{backgroundColor:'green', paddingHorizontal:20, paddingVertical:10}}>
                <Text style={{color:"white"}}>
                   Next
                </Text>
              </TouchableOpacity>
              </Right>

            </View>
          </View>
        </ImageBackground>
              
            
     
    );
  }
}

export default Page7;

