import React, { Component } from 'react';
import { View, Text,ImageBackground,TouchableOpacity} from 'react-native';
import {Form,Item,Input, Container, Content,Button,Left,Right} from 'native-base';
import { observer, inject, Provider } from 'mobx-react';
import validate from 'validate.js';


@inject('BaseStore')
@observer

class Page8 extends Component {
  constructor(props) {
    super(props);
    this.state = {
        email:''
    };
  }
  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}
  async updataData(){
    if(this.validateEmail(this.state.email)){
      if (this.state.email == ''){
        alert('Please Fill Email');
      }else{
        await this.props.BaseStore.infoStore.setEmail(this.state.email)
      
        this.props.navigation.navigate('Page9')
      }
    }else{
      alert('Email Not Valid');
    }
     
   
   
  }
  componentDidMount(){
    let a = this.props.BaseStore.infoStore.email
    this.setState({email:a})
  }

  render() {
    return (
    
        <ImageBackground source={{uri : 'https://png.pngtree.com/thumb_back/fw800/background/20190221/ourmid/pngtree-blue-simple-solid-color-background-color-image_23424.jpg'}} style={{height:"100%",width:'100%',justifyContent:'flex-end'}}>
          <View style={{padding: 20,}} >
           <View >
           <Text style={{color:'white',fontSize:30,alignSelf:"center",marginBottom:10,fontWeight:'bold'}}>
           Enter Your E-mail
            </Text>
         
           
              <Item style={{backgroundColor:"white",borderRadius:10}}>
                <Input placeholder='Enter E-mail' 
                
                
                onChangeText={(text) => this.setState({email:text})} 
                value={this.state.email}

                />
              </Item>
            
            </View>
            <View style={{flexDirection:'row',paddingTop:50}}>
              <Left>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('Page7')} style={{backgroundColor:'red', paddingHorizontal:20, paddingVertical:10}}>
                <Text style={{color:"white"}}>
                   Back
                </Text>
              </TouchableOpacity>
              </Left>
              
              <Right>
              <TouchableOpacity onPress={() => this.updataData()} style={{backgroundColor:'green', paddingHorizontal:20, paddingVertical:10}}>
                <Text style={{color:"white"}}>
                   Next
                </Text>
              </TouchableOpacity>
              </Right>

            </View>
          </View>
        </ImageBackground>
              
            
     
    );
  }
}

export default Page8;

