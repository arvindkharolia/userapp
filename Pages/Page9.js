import React, { Component } from 'react';
import { View, Text,ImageBackground,TouchableOpacity,} from 'react-native';
import {Form,Item,Input, Container, Content,Button,Left,Right,DatePicker} from 'native-base';
import { observer, inject, Provider } from 'mobx-react';


@inject('BaseStore')
@observer

class Page9 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      chosenDate: new Date(),
        
    }
  }
  async updataData(){
    if (this.state.chosenDate == ''){
      alert('Date will be Needed')
    }else{
      await this.props.BaseStore.infoStore.setAdate(this.state.chosenDate)
      this.props.navigation.navigate('Page10')
    }
   
  }
  

  render() {
    return (
    
        <ImageBackground source={{uri : 'https://png.pngtree.com/thumb_back/fw800/background/20190223/ourmid/pngtree-full-purple-solid-color-h5-background-colorbackgroundminimalism-image_87245.jpg'}} style={{height:"100%",width:'100%',justifyContent:'flex-end'}}>
          <View style={{padding:20}}>
           <View >
           <Text style={{color:'white',fontSize:25,alignSelf:"center",marginBottom:10,fontWeight:'bold'}}>
            Tell Us Your Date of Birth            
             </Text>
        
             <Item>
           <DatePicker  placeHolderText="dd/mm/yy" 
                         
                         onDateChange={this.setDate}
                value={this.state.chosenDate}

           />
           </Item>
         
           
            </View>
            <View style={{flexDirection:'row',paddingTop:50}}>
              <Left>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('Page8')} style={{backgroundColor:'red', paddingHorizontal:20, paddingVertical:10}}>
                <Text style={{color:"white"}}>
                   Back
                </Text>
              </TouchableOpacity>
              </Left>
             
              <Right>
              <TouchableOpacity onPress={() => this.updataData()} style={{backgroundColor:'green', paddingHorizontal:20, paddingVertical:10}}>
                <Text style={{color:"white"}}>
                   Next
                </Text>
              </TouchableOpacity>
              </Right>

            </View>
          </View>
        </ImageBackground>
              
            
     
    );
  }
}

export default Page9;

