/**
 * @format
 */

import {AppRegistry} from 'react-native';
import Page2 from './Page2';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => Page2);
