import { observable, action } from "mobx";

class InfoStore{
    @observable name = '';
    @observable username = '';
    @observable password = '';
    @observable fname = '';
    @observable mname = '';
    @observable gndr = '';
    @observable cnumber = '';
    @observable email = '';
    @observable chosenDate = '';
    @observable address = '';
    @observable PickerValueHolder = '';
    @observable pcode = '';
    @observable iuser = '';
    @observable fuser = '';
    @observable coursename = '';
    @observable qual = '';
    @observable duration = '';
    @observable hrname = '';
    @observable collegeid = '';



    
    @action setName = (name) => {
        this.name = name;
    }
    @action setUsername = (username) => {
        this.username = username;
    }
    @action setPassword = (password) => {
        this.password = password;
    }
    @action setFname = (fname) => {
        this.fname = fname;
    }
    @action setMname = (mname) => {
        this.mname = mname;
    }
    @action setGndr = (gndr) => {
        this.gndr = gndr;
    }
    @action setCno = (cnumber) => {
        this.cnumber = cnumber;
    }
    @action setEmail = (email) => {
        this.email = email;
    }
    @action setAdate = (chosenDate) => {
        this.chosenDate = chosenDate;
    }
    @action setAdres = (address) => {
        this.address = address;
    }
    @action setStates = (PickerValueHolder) => {
        this.PickerValueHolder = PickerValueHolder;
    }
    @action setPcode = (pcode) => {
        this.pcode = pcode;
    }
    @action setIuser = (iuser) => {
        this.iuser = iuser;
    }
    @action setFuser = (fuser) => {
        this.fuser = fuser;
    }
    @action setTech = (coursename) => {
        this.coursename = coursename;
    }
    @action setCol = (collegeid) => {
        this.collegeid = collegeid;
    }
    @action setQual = (qual) => {
        this.qual = qual;
    }
    @action setDur = (duration) => {
        this.duration = duration;
    }
    @action setHr = (hrname) => {
        this.hrname = hrname;
    }
    
}   



export default InfoStore;